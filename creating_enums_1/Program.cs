﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creating_enums_1
{
    public class Program
    {

        public enum Rooms
        {
            room1 = 1,  //by using this, I am advising the first element of the enum to have 1 has the starting value.
                        //that means, the successive enums would have a value of 2, 3 and so on...
            room2,
            room3,
            room4,
            room5,
            room6
        }

        public static void Main(string[] args)
        {
            //now, i am going to write a simple code where I will ask for a number to be entered
            //that number will then be checked and the corresponding value displayed

            Console.WriteLine("Enter a number. We wil displaying the corresponding name for you. Also, this is totally lame but you know, learning can be lame so later we can build actually super cool software. ");
            var x = Console.ReadLine(); //collecting the input

            bool room_found = false;

            #region check_room_number

            try
            {
                int y = Convert.ToInt32(x);//converting the input string to number

                //Alright, let's do some comparison stuff. 
                //I am looping through all the enum values. 
                foreach(Rooms room in Enum.GetValues(typeof(Rooms)))
                {
                    if (Convert.ToInt32(room) == y) //this means the entered room number matches the rooms we have
                    {
                        room_found = true;
                        break;//get out of this if if we found the room.
                    }
                    else
                    {
                        room_found = false;  //here I could do away with this else. I have already set the bool as false. Unless the room is found, I dont really have to set this
                        //however, just putting this here for extra clarity      
                    }

                }

                //depending on the bool value, let me display an appropriate value. 
                if(room_found)  //if room found is true
                {
                    Console.WriteLine("We found the room you entered");
                }
                else
                {
                    Console.WriteLine("The room is not there");
                }
            }
            catch(Exception e)  //if something goes wrong during the conversion, I should catch it. 
            {
                Console.WriteLine("Look like something went wrong. Error details are here - {0}", e.ToString());
            }

            #endregion

            Console.ReadLine();
        }
    }
}
